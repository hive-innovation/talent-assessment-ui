import { Button } from "reactstrap";

const Main = () => (
  <div>
    <h1>Occaecat tempor laboris sit reprehenderit eiusmod et.</h1>
    <div>
      Ut amet voluptate nulla nulla laborum veniam nisi proident voluptate
      mollit enim.
      <p>
        In mollit cupidatat sit aliqua. Ad aute veniam reprehenderit ex tempor.
        Sit labore consectetur ea exercitation nisi laboris excepteur minim
        velit excepteur laboris. Labore in aliquip est sint aliqua excepteur
        aute dolore sint tempor consequat deserunt pariatur incididunt. Ex
        laborum deserunt non occaecat proident sit fugiat magna nostrud commodo
        veniam culpa. Officia proident eu non in cillum consectetur officia
        ipsum incididunt irure ut eu nostrud. Esse et minim ipsum tempor veniam
        esse quis.
      </p>
    </div>
    <div className="d-flex justify-content-end">
      <Button color="primary">Button 1</Button>
    </div>
  </div>
);

export default Main;
